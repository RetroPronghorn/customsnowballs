# CustomSnowballs
Create custom snowballs with particle trails & effects that give players potion
effects when tagged.

## Creating a new snowball
Update the `config.yml` with your new custom snowballs. A snowball looks like
```yaml
  party-hard:
    name: Party Hard
    description: I need a toilet...
    effects:
      BLINDNESS: 70
      SLOW: 70
      CONFUSION: 100
      GLOWING: 100
    sounds:
      hit: ENTITY_EXPERIENCE_ORB_PICKUP
      gothit: MUSIC_DISC_WAIT
    particles:
      - SPELL_WITCH
    trail:
      - NOTE
```

References:
 [PotionEfects](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/potion/PotionEffectType.html), 
 [Sounds](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html),
 [Particles](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Particle.html).

## Usage
Once you've created your snowballs you can spawn them in using
```
/customsnowballs spawn <snowball_name> <amount>
```

## Permissions
Spawn a new custom snowball.
```
customsnowballs.spawn
```

## Suggested implementation
This plugin works best when paired with a shop or kit plugin. 
I suggest using the commands to generate the snowballs once then
adding them to PvP kits or shop vendors.