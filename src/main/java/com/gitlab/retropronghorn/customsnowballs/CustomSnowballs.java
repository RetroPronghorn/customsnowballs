/*
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.gitlab.retropronghorn.customsnowballs;

import com.gitlab.retropronghorn.customsnowballs.commands.AutoComplete;
import com.gitlab.retropronghorn.customsnowballs.commands.SpawnSnowball;
import com.gitlab.retropronghorn.customsnowballs.listeners.CustomSnowballListener;
import com.gitlab.retropronghorn.customsnowballs.snowball.CustomSnowball;
import com.gitlab.retropronghorn.customsnowballs.snowball.items.ParticleEnabledSnowball;
import org.bukkit.ChatColor;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/** Represents the CustomSnowballs plugin.
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public final class CustomSnowballs extends JavaPlugin {
    public static CustomSnowballs instance;
    public static FileConfiguration config;

    private HashMap<String, ParticleEnabledSnowball> snowballs = new HashMap<>();

    public static CustomSnowballs getPlugin() {
        return instance;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.config = this.getConfig();
        this.instance = this;

        this.saveDefaultConfig();
        this.registerCustomSnowballs();

        this.getCommand("customsnowballs").setExecutor(new SpawnSnowball(this));
        this.getCommand("customsnowballs").setTabCompleter(new AutoComplete(this));

        this.getServer().getPluginManager().registerEvents(
                new CustomSnowballListener(
                        new CustomSnowball(this, "", "")
                ), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    /**
     * Create a new custom snowball item
     *
     * @param name Name of the custom snowball
     * @param description Description of the custom snowball
     **/
    public static CustomSnowball createSnowball(String name, String description) {
        return new CustomSnowball(instance, name, description);
    }

    private void registerCustomSnowballs() {
        ConfigurationSection customSnowballsConfig = config.getConfigurationSection("snowballs");
        for (String key : customSnowballsConfig.getKeys(false)) {
            ConfigurationSection snowball = customSnowballsConfig.getConfigurationSection(key);
            // Build effects
            HashMap<String, Integer> effects = new HashMap<>();
            for (String effect : snowball.getConfigurationSection("effects").getKeys(false)) {
                effects.put(effect, snowball.getConfigurationSection("effects").getInt(effect));
            }

            ParticleEnabledSnowball customSnowball = new CustomSnowball(instance, snowball.getString("name"), snowball.getString("description")).spawn(1, effects);

            // Build Sounds
            if (snowball.getConfigurationSection("sounds") != null) {
                HashMap<String, String> sounds = new HashMap<>();
                sounds.put("hit", snowball.getString("sounds.hit"));
                sounds.put("gothit", snowball.getString("sounds.gothit"));

                customSnowball.addCustomSounds(sounds);
            }

            // Build Particles
            if (snowball.getStringList("particles") != null) {
                List<String> particles = snowball.getStringList("particles");
                customSnowball.addParticles(particles);
            }

            // Build Particle Trail
            if (snowball.getStringList("trail") != null) {
                List<String> trail = snowball.getStringList("trail");
                customSnowball.addParticleTrail(trail);
            }

            this.snowballs.put(key, customSnowball);
        }
    }

    public ParticleEnabledSnowball getSnowball(String key) {
        return this.snowballs.get(key);
    }

    public List<String> getSnowballs() {
        List<String> snowballNames = new ArrayList<String>(this.snowballs.keySet());
        return snowballNames;
    }

    public static void messagePlayer(Player player, String message) {
        player.sendMessage("[" + ChatColor.AQUA + "CustomSnowballs" + ChatColor.RESET + "]: " + message);
    }
}
