package com.gitlab.retropronghorn.customsnowballs.commands;

import com.gitlab.retropronghorn.customsnowballs.CustomSnowballs;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/** Represents a class that handles CustomSnowball tab completion
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class AutoComplete implements TabCompleter {
    CustomSnowballs instance;

    /** Creates a custom snowball tab complete listener
     * @param instance Instance to the CustomSnowballs plugin
     */
    public AutoComplete(CustomSnowballs instance) {
        this.instance = instance;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        Player senderPlayer = Bukkit.getPlayer(sender.getName());
        if (command.getName().equalsIgnoreCase("customsnowballs")) {
            if (senderPlayer.hasPermission("customsnowballs.spawn") || senderPlayer.isOp()) {
                if (args.length == 1) {
                    List<String> commands = new ArrayList<>();
                    commands.add("spawn");
                    return commands;
                }
                if (args.length == 2 && args[0].equalsIgnoreCase("spawn")) {
                    List<String> snowballNames = new ArrayList<>(instance.getSnowballs());
                    return snowballNames;
                }
                if (args.length == 3 && args[0].equalsIgnoreCase("spawn")) {
                    List<String> numbers = new ArrayList<>();
                    for (int i = 0; i < 16; i++) {
                        numbers.add(Integer.toString(i + 1));
                    }
                    return numbers;
                }
            }
        }
        return null;
    }
}
