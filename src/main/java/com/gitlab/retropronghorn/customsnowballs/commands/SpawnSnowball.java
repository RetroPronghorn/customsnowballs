package com.gitlab.retropronghorn.customsnowballs.commands;

import com.gitlab.retropronghorn.customsnowballs.CustomSnowballs;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/** Represents a class that handles CustomSnowball spawn commands
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class SpawnSnowball implements CommandExecutor {
    public CustomSnowballs instance;

    /** Creates a custom snowball command listener
     * @param instance Instance to the CustomSnowballs plugin
     */
    public SpawnSnowball(CustomSnowballs instance) {
        this.instance = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player senderPlayer = Bukkit.getPlayer(sender.getName());

        if (args.length > 1) {
            if (args[0].equalsIgnoreCase("spawn")) {
                if (senderPlayer.hasPermission("customsnowballs.spawn") || senderPlayer.isOp()) {
                    // Count
                    int amount = 1;
                    String snowballKey = "";
                    if (args.length > 1)
                        snowballKey = args[1];
                    if (instance.getSnowball(snowballKey) == null) {
                        CustomSnowballs.messagePlayer(senderPlayer, ChatColor.YELLOW + "Unknown snowball \"" + snowballKey + "\".");
                        return false;
                    }
                    if (args.length > 2)
                        amount = Integer.parseInt(args[2]);

                    for (int i = 0; i < amount; i++)
                        senderPlayer.getInventory().addItem(instance.getSnowball(snowballKey).getItemStack());
                } else {
                    CustomSnowballs.messagePlayer(senderPlayer, ChatColor.RED + "You do not have the permission customsnowballs.spawn.");
                }
            }
        }
        return false;
    }
}
