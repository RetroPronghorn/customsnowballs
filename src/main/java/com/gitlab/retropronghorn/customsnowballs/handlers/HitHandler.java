package com.gitlab.retropronghorn.customsnowballs.handlers;

import com.gitlab.retropronghorn.customsnowballs.CustomSnowballs;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;

/** Represents a class that handles hit events
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class HitHandler {

    /**
     * Apply potion effects to a player
     *
     * @param player Player to apply the potion effects to
     * @param effectsMeta Effects meta to read from
     **/
    public void applyEffects(Player player, String effectsMeta) {
        HashMap<String, Integer> effects = new HashMap<String, Integer>();
        String[] splitEffects = effectsMeta.split(",");
        for (String effect : splitEffects) {
            String[] oneEffect = effect.split(":");

            if (PotionEffectType.getByName(oneEffect[0]) != null) {
                PotionEffect potionEffect = new PotionEffect(
                        PotionEffectType.getByName(oneEffect[0]),
                        Integer.parseInt(oneEffect[1]),
                        1,
                        true);

                player.addPotionEffect(potionEffect);
            }
        }
    }

    /**
     * Play sounds on target player and attacker
     *
     * @param victim Victim to play sounds on
     * @param attacker Attacker to play sounds on
     **/
    public void dispatchSounds(CustomSnowballs instance, String customSounds, Player victim, Player attacker) {
        Boolean hitSounds = CustomSnowballs.config.getBoolean("sounds.hit.enabled");
        Boolean gothitSounds = CustomSnowballs.config.getBoolean("sounds.gothit.enabled");

        Sound hitSound = Sound.valueOf(CustomSnowballs.config.getString("sounds.hit.sound"));
        Sound gotHitSound = Sound.valueOf(CustomSnowballs.config.getString("sounds.gothit.sound"));

        if (customSounds != null) {
            hitSound = Sound.valueOf(customSounds.split(",")[0]);
            gotHitSound = Sound.valueOf(customSounds.split(",")[1]);
        }

        final Sound finalSound = gotHitSound;

        if (hitSounds)
            attacker.playSound(
                    attacker.getLocation(),
                    hitSound,
                    1f,
                    1f);

        if (gothitSounds) {
            victim.playSound(
                    attacker.getLocation(),
                    finalSound,
                    1f,
                    1f);

            // Cutoff sounds after x amount of time
            instance.getServer().getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
                public void run() {
                    attacker.stopSound(finalSound);
                }
            }, CustomSnowballs.config.getInt("sounds.max_duration"));
        }
    }

    /**
     * Notify both the victim and attacker of events
     *
     * @param victim Victim to notify in chat
     * @param attacker Attacker to notify in chat
     * @param itemName Item name that caused the event
     **/
    public void notifyPlayersInChat(Player victim, Player attacker, String itemName) {
        if (victim.getDisplayName().equals(attacker.getDisplayName())) {
            victim.sendMessage(ChatColor.RED + "You tagged yourself with a " + itemName + "!");
            return;
        }

        victim.sendMessage(ChatColor.YELLOW + attacker.getDisplayName() + " tagged you with a " + itemName + "!");
        attacker.sendMessage(ChatColor.GREEN + "You tagged " + victim.getDisplayName() + " with a " + itemName + "!");
    }

}
