package com.gitlab.retropronghorn.customsnowballs.handlers;


import com.gitlab.retropronghorn.customsnowballs.CustomSnowballs;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/** Represents a class that handles particle events
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class ParticleHandler {

    /**
     * Play a particle trail behind an entity
     *
     * @param instance Instance to the CustomSnowballs class
     * @param entity Entity to play the trail behind
     * @param particles Particle to play
     **/
    public void particleTrail(CustomSnowballs instance, Entity entity, String particles) {
        Location loc = entity.getLocation();
        String[] particlesArray = (particles != null) ? particles.split(",") : new String[]{};
        // Play all particles in the stack
        for (String particle : particlesArray) {
            entity.getWorld().spawnParticle(
                    Particle.valueOf(particle),
                    loc,
                    1,
                    0,
                    0,
                    0);
        }
        instance.getServer().getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
            public void run() {
                if (!entity.isDead())
                    particleTrail(instance, entity, particles);
            }
        }, instance.getConfig().getInt("particle_trail_update"));
    }

    /**
     * Activate new particles on a player when hit
     *
     * @param instance Reference to the CustomSnowballs plugin class
     * @param player Player to particle effects to
     * @param particles Particles to spawn on the player
     **/
    public void playerHitParticle(CustomSnowballs instance, Player player, String particles) {
        if (particles != null) {
            // Duration * 4 (5 ticks) = 20 Ticks aka 1 second
            int repetitions = instance.getConfig().getInt("particle_duration") * 4;
            playerParticleIterator(instance, player, repetitions, particles);
        }
    }


    /**
     * Spawn the particles into the victims world
     *
     * @param player Player to apply the particles to
     * @param particles Particles to apply to the victim
     **/
    public void playParticleStack(Player player, String particles) {
        Location loc = player.getLocation();
        String[] particlesArray = particles.split(",");
        // Play all particles in the stack
        for (String particle : particlesArray) {
            player.getWorld().spawnParticle(
                    Particle.valueOf(particle),
                    loc,
                    CustomSnowballs.config.getInt("player_particle_count"),
                    0,
                    CustomSnowballs.config.getInt("player_particle_offset"),
                    0);
        }
    }


    /**
     * Iterate x amount of times and apply particles to victim
     *
     * @param instance Reference to the CustomSnowballs plugin class
     * @param player Player to apply effects to
     * @param reps repititions to iterate
     * @param particles particles to apply to the victim
     **/
    private void playerParticleIterator(CustomSnowballs instance, Player player, Integer reps, String particles) {
        // Schedule particle effect in 5 ticks.
        instance.getServer().getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
            public void run() {
                playParticleStack(player, particles);
                if (reps > 0)
                    playerParticleIterator(instance, player,reps - 1, particles);
            }
        }, 5);
    }

    /**
     * Iterate x amount of times and play particles on entites
     *
     * @param instance Reference to the CustomSnowballs plugin class
     * @param entity Play particle effects on this entity
     * @param reps repititions to iterate
     * @param particles particles to apply to the victim
     **/
    private void entityParticleIterator(CustomSnowballs instance, Entity entity, Integer reps, String particles) {
        // Schedule particle effect in 5 ticks.
        instance.getServer().getScheduler().scheduleSyncDelayedTask(instance, () -> {
            //playParticleStack(entity, particles);
            if (reps > 0)
                entityParticleIterator(instance, entity,reps - 1, particles);
        }, 5);
    }
}
