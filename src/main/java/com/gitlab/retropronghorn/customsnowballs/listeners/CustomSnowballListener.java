package com.gitlab.retropronghorn.customsnowballs.listeners;

import com.gitlab.retropronghorn.customsnowballs.handlers.HitHandler;
import com.gitlab.retropronghorn.customsnowballs.handlers.ParticleHandler;
import com.gitlab.retropronghorn.customsnowballs.snowball.CustomSnowball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.projectiles.ProjectileSource;

/** Represents the CustomSnowballs listener.
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class CustomSnowballListener implements Listener {
    private CustomSnowball customSnowball;

    /**
     * Create new customSnowball listener events
     *
     * @param customSnowball Custom snowball item to bind events to
     **/
    public CustomSnowballListener(CustomSnowball customSnowball) {
        this.customSnowball = customSnowball;
    }

    @EventHandler
    public void onThrowEntity(ProjectileLaunchEvent event) {
        if (event.getEntity() instanceof Snowball) {
            PersistentDataContainer data = ((Snowball) event.getEntity()).getItem().getItemMeta().getPersistentDataContainer();
            Integer isCustomSnowball = data.get(customSnowball.customIdentifier, PersistentDataType.INTEGER);
            String particleTrailMeta = data.get(customSnowball.trailParticlesIdentifier, PersistentDataType.STRING);
            if (isCustomSnowball != null && isCustomSnowball == 1) {
                Entity snowball = (Snowball) event.getEntity();
                // Apply particle trail to item
                // TODO: Don't do this if the item doesn't have particle effects bound
                ParticleHandler particleTrail = new ParticleHandler();
                particleTrail.particleTrail(customSnowball.customSnowballs, snowball, particleTrailMeta);
            }
        }
    }

    @EventHandler
    public void onEntityHit(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Snowball) {
            // Get item persistent data
            ItemStack attackObject = ((Snowball) event.getDamager()).getItem();
            // Get data container
            PersistentDataContainer data = attackObject.getItemMeta().getPersistentDataContainer();
            // Parse data entries
            Integer isCustomSnowball = data.get(customSnowball.customIdentifier, PersistentDataType.INTEGER);
            String impactParticles = data.get(customSnowball.impactParticlesIdentifier, PersistentDataType.STRING);

            // If this is a Throwable, apply effects
            if (event.getEntity() instanceof Player && isCustomSnowball != null && isCustomSnowball == 1) {
                Player victim = (Player) event.getEntity();
                ProjectileSource attacker = ((Snowball) event.getDamager()).getShooter();
                // Handle Throwable hit effects
                HitHandler hitHandler = new HitHandler();
                ParticleHandler particleHandler = new ParticleHandler();

                if (attacker instanceof Player) {
                    hitHandler.dispatchSounds(
                            customSnowball.customSnowballs,
                            data.get(customSnowball.soundsIdentifier, PersistentDataType.STRING),
                            victim,
                            (Player) attacker);
                    hitHandler.notifyPlayersInChat(
                            victim,
                            (Player) attacker,
                            attackObject.getItemMeta().getDisplayName());

                    // Impact Particles
                    if (impactParticles != null)
                        particleHandler.playerHitParticle(
                                customSnowball.customSnowballs,
                                victim,
                                data.get(customSnowball.impactParticlesIdentifier, PersistentDataType.STRING));
                }

                hitHandler.applyEffects(victim, data.get(customSnowball.effectsIdentifier, PersistentDataType.STRING));
            }
        }
    }
}
