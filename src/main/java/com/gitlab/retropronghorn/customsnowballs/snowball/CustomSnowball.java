package com.gitlab.retropronghorn.customsnowballs.snowball;

import com.gitlab.retropronghorn.customsnowballs.CustomSnowballs;
import com.gitlab.retropronghorn.customsnowballs.snowball.items.ParticleEnabledSnowball;
import org.bukkit.NamespacedKey;

import java.util.HashMap;

/** Represents a custom snowball
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class CustomSnowball {
    public final String material = "SNOWBALL";
    public final NamespacedKey customIdentifier;
    public NamespacedKey effectsIdentifier;
    public NamespacedKey impactParticlesIdentifier;
    public NamespacedKey trailParticlesIdentifier;
    public final NamespacedKey soundsIdentifier;
    public final String name;
    public final String description;

    public CustomSnowballs customSnowballs;

    /**
     * Create a new throwable item
     *
     * @param customSnowballs CustomSnowballs instance
     * @param name DisplayName of the custom snowball
     * @param description Lore description of the custom snowball
     **/
    public CustomSnowball(CustomSnowballs customSnowballs, String name, String description) {
        this.customSnowballs = customSnowballs;
        this.name = name;
        this.description = description;

        this.customIdentifier = new NamespacedKey(customSnowballs, "throwable");
        this.effectsIdentifier = new NamespacedKey(customSnowballs, "effects");
        this.impactParticlesIdentifier = new NamespacedKey(customSnowballs, "impact_particles");
        this.trailParticlesIdentifier = new NamespacedKey(customSnowballs, "trail_particles");
        this.soundsIdentifier = new NamespacedKey(customSnowballs, "sounds");
    }

    /**
     * Spawn a new throwable item with one effect
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effect Effect to apply when hit
     * @param duration How long the effect should last when activated
     **/
    public ParticleEnabledSnowball spawn(Integer count, String effect, Integer duration) {
        // Create meta HashMap
        HashMap<String, String> meta = new HashMap<String, String>();
        meta.put("material", this.material);
        meta.put("name", this.name);
        meta.put("description", this.description);
        // Put effect into HashMap
        HashMap<String, Integer> effects = new HashMap<String, Integer>();
        effects.put(effect, duration);

        return (new ParticleEnabledSnowball(this, meta, effects, count));
    }

    /**
     * Spawn a new throwable item with multiple effects
     *
     * @param count Integer amount to spawn in the itemstack
     * @param effects List of the effects, and duration of the effects
     **/
    public ParticleEnabledSnowball spawn(Integer count, HashMap<String, Integer> effects) {
        // Create MetaHashmap
        HashMap<String, String> meta = new HashMap<String, String>();
        meta.put("material", this.material);
        meta.put("name", this.name);
        meta.put("description", this.description);

        return new ParticleEnabledSnowball(this, meta, effects, count);
    }
}
