package com.gitlab.retropronghorn.customsnowballs.snowball.items;

import com.gitlab.retropronghorn.customsnowballs.snowball.CustomSnowball;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Represents an effect enabled snowball
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class EffectEnabledSnowball {
    CustomSnowball instance;
    Material material;
    ItemStack item;
    HashMap<String, Integer> effects;

    /**
     * Create a new throwable entity
     *
     * @param instance Instance of the throwable item class
     * @param meta Metadata to attach to the throwable entity
     * @param effects Effects to apply on contact with victim
     * @param count Amount of the throwable entity to add to the stack
     **/
    public EffectEnabledSnowball(CustomSnowball instance, HashMap<String, String> meta, HashMap<String, Integer> effects, Integer count) {
        this.instance = instance;
        this.material = Material.getMaterial(meta.get("material"));
        this.effects = effects;
        this.item = new ItemStack(material, count);

        this.setDisplayName(meta.get("name"));
        this.setDescription(meta.get("description"));

        // Store the tag on the item
        tagAsCustomSnowball();
        registerEffects();
    }

    /**
     * Set the description lore
     *
     * @param name Display name of the item
     **/
    public void setDisplayName(String name) {
        ItemMeta meta = this.item.getItemMeta();
        meta.setDisplayName(name);
        this.item.setItemMeta(meta);
    }

    /**
     * Set the description lore
     *
     * @param description Description of the custom snowball
     **/
    public void setDescription(String description) {
        ItemMeta meta = this.item.getItemMeta();
        List<String> lore = new ArrayList<>();
        if (this.item.getLore() != null)
            lore = this.item.getLore();
        if (lore.isEmpty()) {
            lore.add(description);
        } else {
            lore.set(0, description);
        }
        meta.setLore(lore);
        this.item.setItemMeta(meta);
    }

    /**
     * Tag the item as custom snowball in persistent storage
     **/
    public void tagAsCustomSnowball() {
        NamespacedKey key = instance.customIdentifier;
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.INTEGER, 1);
        item.setItemMeta(itemMeta);
    }

    /**
     * Register effects to persistent storage on the item
     **/
    public void registerEffects() {
        String effectsMeta = "";
        for (Map.Entry<String, Integer> effect : this.effects.entrySet()) {
            effectsMeta += effect.getKey() + ":" + effect.getValue() + ",";
        }
        effectsMeta = effectsMeta.substring(0, effectsMeta.length() - 1);

        NamespacedKey key = this.instance.effectsIdentifier;
        ItemMeta itemMeta = this.item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, effectsMeta);
        this.item.setItemMeta(itemMeta);
    }

    /**
     * Get the throwable ItemStack reference
     *
     * @return ItemStack itemstack reference
     **/
    public ItemStack getItemStack() {
        return item;
    }
}
