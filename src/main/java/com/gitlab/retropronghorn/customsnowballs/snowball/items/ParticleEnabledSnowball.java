package com.gitlab.retropronghorn.customsnowballs.snowball.items;

import com.gitlab.retropronghorn.customsnowballs.snowball.CustomSnowball;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.HashMap;
import java.util.List;

/** Represents a particle enabled snowball
 * @author RetroPronghorn
 * @author https://gitlab.com/retropronghorn/customsnowballs
 * @version 1.0-SNAPSHOT
 * @since 1.0
 */
public class ParticleEnabledSnowball extends EffectEnabledSnowball {
    List<String> particles;
    List<String> particleTrail;

    /**
     * Create a new throwable entity
     *
     * @param instance Instance of the throwable item class
     * @param meta Metadata to attach to the throwable entity
     * @param effects Effects to apply on contact with victim
     * @param count Amount of the throwable entity to add to the stack
     **/
    public ParticleEnabledSnowball(CustomSnowball instance, HashMap<String, String> meta, HashMap<String, Integer> effects, Integer count) {
        super(instance, meta, effects, count);
    }

    /**
     * Add a impact particle to the snowball meta
     *
     * @param particles List of particles to add
     **/
    public ParticleEnabledSnowball addParticles(List<String> particles) {
        this.particles = particles;
        this.registerImpactParticles();
        return this;
    }

    /**
     * Add a particle trail to the snowball meta
     *
     * @param particles List of particles to add
     **/
    public ParticleEnabledSnowball addParticleTrail(List<String> particles) {
        this.particleTrail = particles;
        this.registerParticleTrail();
        return this;
    }

    /**
     * Add a custom sounds to hit and gothit events
     *
     * @param sounds List of sounds to override
     **/
    public ParticleEnabledSnowball addCustomSounds(HashMap<String, String> sounds) {
        String soundsMeta = "";
        soundsMeta += sounds.get("hit") + ",";
        soundsMeta += sounds.get("gothit");
        NamespacedKey key = this.instance.soundsIdentifier;
        ItemMeta itemMeta = this.item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, soundsMeta);
        this.item.setItemMeta(itemMeta);
        return this;
    }

    /**
     * Register impact particles to NBT data
     **/
    public void registerImpactParticles() {
        String particlesMeta = "";
        for (String particle : this.particles) {
            particlesMeta += particle + ",";
        }
        particlesMeta = particlesMeta.substring(0, particlesMeta.length() - 1);

        NamespacedKey key = this.instance.impactParticlesIdentifier;
        ItemMeta itemMeta = this.item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, particlesMeta);
        this.item.setItemMeta(itemMeta);
    }

    /**
     * Register particle trail to NBT data
     **/
    public void registerParticleTrail() {
        String particlesMeta = "";
        for (String particle : this.particleTrail) {
            particlesMeta += particle + ",";
        }
        particlesMeta = particlesMeta.substring(0, particlesMeta.length() - 1);

        NamespacedKey key = this.instance.trailParticlesIdentifier;
        ItemMeta itemMeta = this.item.getItemMeta();
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, particlesMeta);
        this.item.setItemMeta(itemMeta);
    }
}
